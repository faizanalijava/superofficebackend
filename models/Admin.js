var mongoose=require('mongoose');
var Schema=mongoose.Schema;
var bcrypt=require('bcrypt');
var AdminSchema=new Schema({
    email:{
        type:String,
        required:(true,'Email is Must'),
        unique:true
    },
    password:{
        type:String,
        required:(true,'Password is must')
    },
    office_location:{
        office_lat:{
            type:Number,
            default:24.878722
        },
        office_lng:{
            type:Number,
            default:67.062977
        }
    }

});
AdminSchema.methods.comparePassword=function (password) {
    return bcrypt.compareSync(password, this.password);
};
var Admin=mongoose.model('admin',AdminSchema);
module.exports=Admin;
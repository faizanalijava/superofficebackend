var Admin=require('../models/Admin');
var User=require('../models/User');
var Location=require('../models/Location');
var jwt = require('jsonwebtoken');
var bcrypt=require('bcrypt');
const crypto = require('crypto');
var Cryptr = require('cryptr'),
    cryptr = new Cryptr('superoffice');
var requestify = require('requestify');
const secret = 'q9kvsKOsUhtUq8j';
var app_id="67172";
var auth_key="GjEmrff-2XJDFO2";
var base_url='https://api.quickblox.com/';
exports.addNewAdmin=function (req,res,next) {
    if(req.body.email&&req.body.password){
        var admin=new Admin(req.body);
        //admin.password = bcrypt.hashSync(req.body.password, 10);
        admin.password = cryptr.encrypt(req.body.password),
        admin.save(function (err,admin) {
            if(err){
                return res.send({response:{status :400,message: err.errmsg}
                });
            }else{
                return res.send({response:{status :200,message: "Admin Successfully Added"}});

            }

        })
    }
    else{
        return res.send({response:{status :400,message: "Data is not valid"}});
    }
};
exports.signin=function (req,res) {
    if(req.body.email&&req.body.password){
        Admin.findOne({email:req.body.email},function (err,admin) {
            if (err){
                res.json({response:{status :301,message: err}});
            }
            if (!admin ||cryptr.decrypt(admin.password)!=req.body.password) {
                return res.json({response:{ status :301 ,message: 'Authentication failed. Invalid user or password.'} });
            }
            var currentTime=Math.round(new Date().getTime()/1000);
            const hash = crypto.createHmac('sha1', secret)
                .update('application_id=67172&auth_key=GjEmrff-2XJDFO2&nonce='
                    +currentTime+'&timestamp='+currentTime)
                .digest('hex');
            var data = {"application_id":app_id,"auth_key":auth_key,"nonce":currentTime,
            "timestamp":currentTime,"signature":hash};
            var config = {};
            config.timeout = 60000;
            requestify.request(base_url+'session.json', {
                method: 'POST',
                body: data,
                dataType: 'json'

            },config)
                .then(function(resp) {
                    return res.json({response:{ status :200}
                        ,result:{token: 'JWT '+jwt.sign({id:admin._id,qb_token: resp.getBody().session.token,}, 'super')}});
                })
                .fail(function(fail) {
                    console.log("fail",fail.getBody());// Some error code such as, for example, 404
                    return res.send({response:{status:fail.getCode(),message:'qb error'}})
                });

        });
    }
    else{
        res.send({response:{status :301,message:"Email or password can not be null"}});
    }
}
exports.getallUsers=function (req,res) {
    //res.send(req.user);
    var usersProjection = {
        __v: false,
        password: false
    };
    if(req.user) {
        User.find({},usersProjection,function (err,user) {
            if(err){
                res.send({response: {status: 400, message: err}});
            }
            else{
                res.send({response: {status: 200, message: "Success"},result:{users:user}});
            }
        }).sort({name:'asc'});
    }
    else{
        res.send({response: {status:400,message: 'Please Login First'}});
    }
};
exports.addNewUser=function (req,res,next) {
    var headers= { 'QuickBlox-REST-API-Version': "0.1.1",
        'QB-Account-Key': "1AKQRu-EPTtPxzxxDrBo",
        'QB-Token': req.user.qb_token,
        'Content-Type':'application/json'
    };
    // var data='application_id=67172&auth_key=GjEmrff-2XJDFO2&nonce=1517820480 &timestamp=1517820480';
    // var sig=crypto.createHmac('sha1', 'q9kvsKOsUhtUq8j').update(data).digest('hex')
    // console.log(sig);
    if(req.body.email&&req.body.password) {
        if(req.body.user_type=='Boss'){
            User.count({user_type:'rehan'}, function( err, count){
                if(count>0){return res.send({response:{status :400,message: "Boss Already Exist"}});}
                else{
                    req.body.user_type='rehan';
                    adduser(req,headers,res);
                }

            })
        }
        else {
            req.body.user_type='employee';
            adduser(req, headers, res);
        }
    }
    else{
        return res.send({response:{status :400,message: "Data is not valid"}});
    }

    // User.create(user).then(function (user) {
    //     res.send(user);
    // }).catch(next)

};
exports.updateUser=function (req,res,next) {

    if(req.body.email&&req.body.name) {

                var headers= { 'QuickBlox-REST-API-Version': "0.1.1",
                    'QB-Account-Key': "1AKQRu-EPTtPxzxxDrBo",
                    'QB-Token': req.body.qb_token,
                    'Content-Type':'application/json'
                };
                var data = {"user":{"id":req.body.qb_id,"login":req.body.email
                    ,"full_name":req.body.name}};
                requestify.request(base_url+'users/'+req.body.qb_id+'.json', {
                    method: 'PUT',
                    body: data,
                    headers: headers,
                    dataType: 'json'
                })
                    .then(function(resp) {
                        // get the response body
                        var qbUser=resp.getBody().user;
                        User.update({_id:req.body.id},{ name:req.body.name,
                            email:req.body.email,
                        },function (err,affect) {
                            if(err){
                                return res.send({response:{status :400,message: err.errmsg}});
                            }
                            return res.send({response:{status :200,message: 'success'}});

                        });
                    })
                    .fail(function(fail) {
                        console.log("fail",fail.getBody());// Some error code such as, for example, 404
                        res.send({response:{status:fail.getCode(),message:'qb error'}})
                    });



    }
    else{
        return res.send({response:{status :400,message: "Data is not valid"}});
    }

    // User.create(user).then(function (user) {
    //     res.send(user);
    // }).catch(next)

};
exports.deleteUser=function (req,res) {
    if(req.user){
        if(!req.body.user_id){
            return res.send({response:{status:400,message:'user id is required'}});
        }
        User.findOne({_id:req.body.user_id},function (err,user) {
            if(err){
                res.send({response: {status: 400, message: err.message}});
            }
            else{
                var currentTime=Math.round(new Date().getTime()/1000);
                const hash = crypto.createHmac('sha1', secret)
                    .update('application_id=67172&auth_key=GjEmrff-2XJDFO2&nonce='
                        +currentTime+'&timestamp='+currentTime+
                        "&user[login]="+user.email
                        +"&user[password]="+cryptr.decrypt(user.password))
                    .digest('hex');
                var parentData = {"application_id":app_id,"auth_key":auth_key,"nonce":currentTime,
                    "timestamp":currentTime,"signature":hash,"user":{"login":user.email
                        ,"password":cryptr.decrypt(user.password)}};
                requestify.request(base_url+'session.json', {
                    method: 'POST',
                    body: parentData,
                    dataType: 'json'
                })
                    .then(function(resp) {
                        var headers= { 'QuickBlox-REST-API-Version': "0.1.1",
                            'QB-Account-Key': "1AKQRu-EPTtPxzxxDrBo",
                            'QB-Token': resp.getBody().session.token,
                            'Content-Type':'application/json'
                        };
                        requestify.request(base_url+'users/'+user.qb_id+'.json', {
                            method: 'Delete',
                            headers: headers,
                            dataType: 'json'
                        })
                            .then(function(resp) {
                                User.findByIdAndRemove({_id:req.body.user_id},function (err) {
                                    if(err){
                                        return res.send({response:{status:400,message:err.message}});
                                    }
                                    return res.send({response:{status:200,message:'success'}});
                                })
                            })
                            .fail(function(fail) {
                                console.log("fail",fail.getBody());// Some error code such as, for example, 404
                                res.send({response:{status:fail.getCode(),message:'qb error'}})
                            });

                    })
                    .fail(function(fail) {
                        console.log("fail1",fail.getBody());// Some error code such as, for example, 404
                        res.send({response:{status:fail.getCode(),message:'qb error'}})
                    });

            }
        })


    }
    else{
        return res.send({response:{status:401,message:'you are not authorized to perform this task'}});
    }
}
exports.changePasswordUser=function (req,res,next) {

    if(req.body.password) {

        var headers= { 'QuickBlox-REST-API-Version': "0.1.1",
            'QB-Account-Key': "1AKQRu-EPTtPxzxxDrBo",
            'QB-Token': req.body.qb_token,
            'Content-Type':'application/json'
        };
        var data = {"user":{"id":req.body.qb_id,"old_password":req.body.old_password
            ,"password":req.body.password}};
        requestify.request(base_url+'users/'+req.body.qb_id+'.json', {
            method: 'PUT',
            body: data,
            headers: headers,
            dataType: 'json'
        })
            .then(function(resp) {
                User.update({_id:req.body.id},{ password:cryptr.encrypt(req.body.password)
                },function (err,affect) {
                    if(err){
                        return res.send({response:{status :400,message: err.errmsg}});
                    }
                    return res.send({response:{status :200,message: 'success'}});

                });
            })
            .fail(function(fail) {
                console.log("fail",fail.getBody());// Some error code such as, for example, 404
                res.send({response:{status:fail.getCode(),message:'qb error'}})
            });



    }
    else{
        return res.send({response:{status :400,message: "Data is not valid"}});
    }

    // User.create(user).then(function (user) {
    //     res.send(user);
    // }).catch(next)

};
exports.getOfficelocation=function (req,res) {
    //res.send(req.user);
    var adminProjection = {
        __v: false,
        password: false,
        _id: false,
        email: false,
    };
    if(req.user) {
        Admin.findOne({_id:req.user.id},adminProjection,function (err,admin) {
            if(err){
                res.send({response: {status: 400, message: err}});
            }
            else{
                res.send({response: {status: 200, message: "Success"},result:{location:admin.office_location}});
            }
        })
    }
    else{
        res.send({response: {status:400,message: 'Please Login First'}});
    }
}
exports.getSingleUser=function (req,res) {
    //res.send(req.user);
    var userProjection = {
        __v: false
    };
    if(req.user) {
        User.findOne({_id:req.body.id},userProjection,function (err,user) {
            if(err){
                res.send({response: {status: 400, message: err.message}});
            }
            else{
                var currentTime=Math.round(new Date().getTime()/1000);
                const hash = crypto.createHmac('sha1', secret)
                    .update('application_id=67172&auth_key=GjEmrff-2XJDFO2&nonce='
                        +currentTime+'&timestamp='+currentTime+
                        "&user[login]="+user.email
                        +"&user[password]="+cryptr.decrypt(user.password))
                    .digest('hex');
                var parentData = {"application_id":app_id,"auth_key":auth_key,"nonce":currentTime,
                    "timestamp":currentTime,"signature":hash,"user":{"login":user.email
                        ,"password":cryptr.decrypt(user.password)}};
                requestify.request(base_url+'session.json', {
                    method: 'POST',
                    body: parentData,
                    dataType: 'json'
                })
                    .then(function(resp) {
                        res.send({response: {status: 200, message: "Success"},
                            result:{_id:user._id,email:user.email,name:user.name,
                            qb_token:resp.getBody().session.token,qb_id:user.qb_id
                                ,password:cryptr.decrypt(user.password)}});

                    })
                    .fail(function(fail) {
                        console.log("fail1",fail.getBody());// Some error code such as, for example, 404
                        res.send({response:{status:fail.getCode(),message:'qb error'}})
                    });

            }
        })
    }
    else{
        res.send({response: {status:400,message: 'Please Login First'}});
    }
}
exports.updateOfficeLocation=function (req,res) {
    if(req.user){
        if(!req.body.lat||!req.body.lng){
            return res.send({response:{status:400,message:'Lat and lng required'}});
        }

        Admin.update({_id:req.user.id},{office_location:{
            office_lat:req.body.lat,
            office_lng:req.body.lng
        }},function (err,affect) {
            if (err) {
                return  res.send({response: {status:400,message: err}});
            }
            Location.update({}, {
                office_location: {
                    office_lat:req.body.lat,
                    office_lng:req.body.lng
                }
            },{multi: true}, function (err, affected) {
                if (err) {
                    res.send({response: {status:400,message: err}});
                }
                else {
                    return  res.send({response: {status:200,message: 'Location Update Successfully'}});
                }
            });


        });
    }
    else{
        return res.send({response:{status:401,message:'you are not authorized to perform this task'}});
    }
}
function adduser(req,headers,res) {
    var user=new User(req.body);
    user.password = cryptr.encrypt(req.body.password);
    var data = {"user":{"login":user.email
        ,"password":req.body.password,"full_name":user.name}};
    requestify.request('https://api.quickblox.com/users.json', {
        method: 'POST',
        body: data,
        headers: headers,
        dataType: 'json'
    })
        .then(function(resp) {
            // get the response body
            var qbUser=resp.getBody().user;
            user.save(function (err,user) {
                if(err){
                    return res.send({response:{status :400,message: err.errmsg}

                    });
                }else{

                    Admin.findOne({_id:req.user.id},function (err,admin) {
                        var location=new Location();
                        location.user_id=user._id;
                        location.office_location.office_lat=admin.office_location.office_lat;
                        location.office_location.office_lng=admin.office_location.office_lng;
                        location.save(function (err,location) {
                            if(err){
                                return res.send({response:{status :400,message: err.errmsg}});
                            }
                            User.update({_id:user._id},{
                                locations:location._id,qb_id:qbUser.id
                            },function (err,affect) {
                                if(err){
                                    return res.send({response:{status :400,message: err.errmsg}});
                                }


                                user.password = 'null';
                                return res.json({
                                    response:{status :200,message:
                                        "User inserted Succesfully"},result:{
                                        user_id:user._id,
                                        name:user.name,
                                        email:user.email,
                                        qb:resp.getBody().user
                                    }});
                            });

                        });
                    });


                }

            })

        })
        .fail(function(fail) {
            console.log("fail",fail.getBody());// Some error code such as, for example, 404
            res.send({response:{status:fail.getCode(),message:'qb error'}})
        });


}
var token=localStorage.getItem('token');
var last=localStorage.getItem('time');
var current=new Date().getTime();
if(token==null){
    window.location="../index.html";
}
else{
    var afterTime=current-last;
    if((afterTime)>3600000 ){
        localStorage.clear();
        window.location="../index.html";
    }
    else{
        afterTime=3600000 -afterTime;
        setTimeout(function () {
            localStorage.clear();
            window.location="../index.html";
        }, afterTime);
    }
}